<?php

namespace TheFeed\Controleur;


use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use Symfony\Component\HttpFoundation\Response;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;
use TheFeed\Service\PublicationServiceInterface;
use TheFeed\Service\UtilisateurServiceInterface;
use function PHPUnit\Framework\returnArgument;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ControleurPublication extends ControleurGenerique
{
    private PublicationServiceInterface $publicationService;


    /**
     * @param PublicationServiceInterface $publicationService
     */
    public function __construct( ContainerInterface $container, PublicationServiceInterface $publicationService )
    {
        parent::__construct($container);
        $this->publicationService = $publicationService;
    }


    #[Route(path: '/', name:'afficherListeTemporaire')]
    #[Route(path: '/publications', name:'afficherListe', methods:["GET"])]
    public function afficherListe() : Response
    {
        $publications = $this->publicationService->recupererPublications();
        return $this->afficherTwig(
            'publication/feed.html.twig', [
                "publications" => $publications,
            ]);
    }

    #[Route(path: '/publications', name:'creerDepuisFormulairePublication', methods:["POST"])]
    public function creerDepuisFormulaire() : Response
    {
        //MessageFlash::ajouter("success", "test");

        $idUtilisateurConnecte = ConnexionUtilisateur::getIdUtilisateurConnecte();
        $message = $_POST['message'];
        try {
            $this->publicationService->creerPublication($idUtilisateurConnecte, $message);
        }
        catch(ServiceException $e) {
            MessageFlash::ajouter("error", $e->getMessage());
        }

        return $this->rediriger('afficherListe');
    }


}