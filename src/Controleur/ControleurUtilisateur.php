<?php

namespace TheFeed\Controleur;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Configuration\Configuration;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;
use TheFeed\Service\PublicationServiceInterface;
use TheFeed\Service\UtilisateurService;
use TheFeed\Service\UtilisateurServiceInterface;

class ControleurUtilisateur extends ControleurGenerique
{

    private PublicationServiceInterface $publicationService;
    private UtilisateurServiceInterface $utilisateurService;


    /**
     * @param PublicationServiceInterface $publicationService
     * @param UtilisateurServiceInterface $utilisateurService
     */
    public function __construct(ContainerInterface $container, PublicationServiceInterface $publicationService, UtilisateurServiceInterface $utilisateurService)
    {
        parent::__construct($container);
        $this->publicationService = $publicationService;
        $this->utilisateurService = $utilisateurService;
    }

    public function afficherErreur($messageErreur = "", $controleur = ""): Response
    {
        return parent::afficherErreur($messageErreur, "utilisateur");
    }

    #[Route(path: '/utilisateurs/{idUtilisateur}/publications', name:'afficherPublications', methods:["GET"])]
    public function afficherPublications($idUtilisateur): Response
    {
        try {
            $utilisateur = $this->utilisateurService->recupererUtilisateurParId($idUtilisateur);
            $publications = $this->publicationService->recupererPublicationsUtilisateur($idUtilisateur);
        } catch (ServiceException $e) {
            MessageFlash::ajouter("error", $e->getMessage());
            return $this->rediriger("afficherListe");
        }

        $login = $utilisateur->getLogin();
        return $this->afficherTwig('publication/page_perso.html.twig', [
            "publications" => $publications,
            "login" => $login
        ]);
    }

    #[Route(path: '/inscription', name:'afficherFormulaireCreation', methods:["GET"])]
    public function afficherFormulaireCreation(): Response
    {
        return $this->afficherTwig('utilisateur/inscription.html.twig', [
            "method" => Configuration::getDebug() ? "get" : "post",
        ]);
    }

    #[Route(path: '/inscription', name:'creerDepuisFormulaireUtilisateur', methods:["POST"])]
    public function creerDepuisFormulaire(): Response
    {
        $login = $_POST['login'] ?? null;
        $motDePasse = $_POST['mot-de-passe'] ?? null;
        $adresseMail = $_POST['email'] ?? null;
        $nomPhotoDeProfil = $_FILES['nom-photo-de-profil'] ?? null;

        try {
            $this->utilisateurService->creerUtilisateur($login, $motDePasse, $adresseMail, $nomPhotoDeProfil);
        } catch (ServiceException $e) {
            MessageFlash::ajouter("error", $e->getMessage());
        }

        MessageFlash::ajouter("success", "L'utilisateur a bien été créé !");
        return $this->rediriger("afficherListe");
    }

    #[Route(path: '/connexion', name:'afficherFormulaireConnexion', methods:["GET"])]
    public function afficherFormulaireConnexion(): Response
    {
        return $this->afficherTwig('utilisateur/connexion.html.twig');
    }

    #[Route(path: '/connexion', name:'connecter', methods:["POST"])]
    public function connecter(): Response
    {
        $login = $_POST['login'] ?? null;
        $motDePasse = $_POST['mot-de-passe'] ?? null;

        try {
            $this->utilisateurService->connecterUtilisateur($login, $motDePasse);
        }
        catch (ServiceException $e) {
            MessageFlash::ajouter("error", $e->getMessage());
            return $this->rediriger("afficherFormulaireConnexion");
        }

        MessageFlash::ajouter("success", "Connexion effectuée.");
        return $this->rediriger("afficherListe");
    }

    #[Route(path: '/deconnexion', name:'deconnecter', methods:["GET"])]
    public function deconnecter(): Response
    {
        if (!ConnexionUtilisateur::estConnecte()) {
            MessageFlash::ajouter("error", "Utilisateur non connecté.");
            return $this->rediriger("afficherListe");
        }
        ConnexionUtilisateur::deconnecter();
        MessageFlash::ajouter("success", "L'utilisateur a bien été déconnecté.");
        return $this->rediriger("afficherListe");
    }
}
