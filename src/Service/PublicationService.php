<?php

namespace TheFeed\Service;

use TheFeed\Controleur\ControleurUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\PublicationRepositoryInterface;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;
use function PHPUnit\Framework\returnArgument;
use Symfony\Component\HttpFoundation\Response;


class PublicationService implements PublicationServiceInterface
{

    private PublicationRepositoryInterface $publicationRepository;
    private UtilisateurRepositoryInterface $utilisateurRepository;

    /**
     * @param PublicationRepositoryInterface $publicationRepository
     * @param UtilisateurRepositoryInterface $utilisateurRepository
     */
    public function __construct(
        PublicationRepositoryInterface $publicationRepository,
        UtilisateurRepositoryInterface $utilisateurRepository )
    {
        $this->publicationRepository = $publicationRepository;
        $this->utilisateurRepository = $utilisateurRepository;
    }


    public function recupererPublications() : array
    {
        return $this->publicationRepository->recuperer();
    }

    /**
     * @throws ServiceException
     */
    public function creerPublication($idUtilisateur, $message) : void
    {
        $utilisateur = $this->utilisateurRepository->recupererParClePrimaire($idUtilisateur);

        if ($utilisateur == null) {
            throw new ServiceException("Il faut être connecté pour publier un feed");
        }
        if ($message == null || $message == "") {
            throw new ServiceException("Le message ne peut pas être vide!");
        }
        if (strlen($message) > 250) {
            throw new ServiceException("Le message ne peut pas dépasser 250 caractères!");
        }

        $publication = Publication::create($message, $utilisateur);
        $this->publicationRepository->ajouter($publication);
    }

    public function recupererPublicationsUtilisateur($idUtilisateur) : array
    {
        return $this->publicationRepository->recupererParAuteur($idUtilisateur);
    }

    /**
     * @throws ServiceException
     */
    public function supprimerPublication(int $idPublication, ?string $idUtilisateurConnecte): void
    {
        $publication = $this->publicationRepository->recupererParClePrimaire($idPublication);

        if (is_null($idUtilisateurConnecte))
            throw new ServiceException("Il faut être connecté pour supprimer une publication", Response::HTTP_FORBIDDEN);

        if ($publication === null)
            throw new ServiceException("Publication inconnue.", Response::HTTP_NOT_FOUND);

        if ($publication->getAuteur()->getIdUtilisateur() !== intval($idUtilisateurConnecte))
            throw new ServiceException("Seul l'auteur de la publication peut la supprimer", Response::HTTP_UNAUTHORIZED);

        $this->publicationRepository->supprimer($publication);
    }

    /**
     * @throws ServiceException
     */
    public function recupererPublicationParId($idPublication, $autoriserNull = true) : ?Publication {
        $publication = $this->publicationRepository->recupererParClePrimaire($idPublication);
        if(!$autoriserNull && $publication == null) {
            throw new ServiceException("La publication n'existe pas.", Response::HTTP_NOT_FOUND);
        }
        return $publication;
    }
}