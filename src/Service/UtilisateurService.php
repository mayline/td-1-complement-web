<?php

namespace TheFeed\Service;



use Symfony\Component\HttpFoundation\Response;
use TheFeed\Controleur\ControleurUtilisateur;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepositoryInterface;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;
use function PHPUnit\Framework\isNull;

class UtilisateurService implements UtilisateurServiceInterface
{

    private UtilisateurRepositoryInterface $utilisateurRepository;
    private string $dossierPhotoDeProfil;
    private FileMovingServiceInterface $fileMovingService;

    /**
     * @param UtilisateurRepositoryInterface $utilisateurRepository
     * @param string $dossierPhotoDeProfil
     * @param FileMovingServiceInterface $fileMovingService
     */
    public function __construct( UtilisateurRepositoryInterface $utilisateurRepository, string $dossierPhotoDeProfil, FileMovingServiceInterface $fileMovingService )
    {
        $this->utilisateurRepository = $utilisateurRepository;
        $this->dossierPhotoDeProfil = $dossierPhotoDeProfil;
        $this->fileMovingService = $fileMovingService;
    }

    /**
     * @throws ServiceException
     */
    public function creerUtilisateur($login, $motDePasse, $adresseMail, $nomPhotoDeProfil): void {
        if (is_null($login) || is_null($motDePasse) || is_null($adresseMail) || is_null($nomPhotoDeProfil)) {
            throw new ServiceException("Login, nom, prenom ou mot de passe manquant.");
        }
        if (strlen($login) < 4 || strlen($login) > 20) {
            throw new ServiceException("Le login doit être compris entre 4 et 20 caractères!");
        }
        if (!preg_match("#^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,20}$#", $motDePasse)) {
            throw new ServiceException("Mot de passe invalide!");
        }
        if (!filter_var($adresseMail, FILTER_VALIDATE_EMAIL)) {
            throw new ServiceException("L'adresse mail est incorrecte!");
        }

        $utilisateurRepository = $this->utilisateurRepository;
        $utilisateur = $utilisateurRepository->recupererParLogin($login);
        if ($utilisateur != null) {
            throw new ServiceException("Ce login est déjà pris!");
        }

        $utilisateur = $utilisateurRepository->recupererParEmail($adresseMail);
        if ($utilisateur != null) {
            throw new ServiceException("Un compte est déjà enregistré avec cette adresse mail!");
        }

        $mdpHache = MotDePasse::hacher($motDePasse);

        // Upload des photos de profil
        // Plus d'informations :
        // http://romainlebreton.github.io/R3.01-DeveloppementWeb/assets/tut4-complement.html

        // On récupère l'extension du fichier
        $explosion = explode('.', $nomPhotoDeProfil['name']);
        $fileExtension = end($explosion);
        if (!in_array($fileExtension, ['png', 'jpg', 'jpeg'])) {
            throw new ServiceException("La photo de profil n'est pas au bon format!");
        }
        // La photo de profil sera enregistrée avec un nom de fichier aléatoire
        $pictureName = uniqid() . '.' . $fileExtension;
        $from = $nomPhotoDeProfil['tmp_name'];
        //$to = __DIR__ . "/../../ressources/img/utilisateurs/$pictureName";
        $to = $this->dossierPhotoDeProfil . "$pictureName";
        printf($to);
        $this->fileMovingService->moveFile($from, $to);

        $utilisateur = Utilisateur::create($login, $mdpHache, $adresseMail, $pictureName);
        $utilisateurRepository->ajouter($utilisateur);
    }

    /**
     * @throws ServiceException
     */
    public function recupererUtilisateurParId($idUtilisateur, $autoriserNull = true) : ?Utilisateur {
         $utilisateur = $this->utilisateurRepository->recupererParClePrimaire($idUtilisateur);
         if(!$autoriserNull && is_null($utilisateur)) {
                throw new ServiceException("l’utilisateur sélectionné n’existe pas", Response::HTTP_NOT_FOUND);
         }
         return $utilisateur;
    }

    /**
     * @throws ServiceException
     */
    public function connecterUtilisateur($login, $motDePasse): void {
        if (is_null($login) || is_null($motDePasse)) {
            throw new ServiceException("Login ou mot de passe manquant.");
        }

        $utilisateurRepository = $this->utilisateurRepository;
        /** @var Utilisateur $utilisateur */
        $utilisateur = $utilisateurRepository->recupererParLogin($login);

        if (is_null($utilisateur)) {
            throw new ServiceException("Login inconnu.");
        }
        if (!MotDePasse::verifier($motDePasse, $utilisateur->getMdpHache())) {
            throw new ServiceException("Mot de passe incorrect.");
        }

        ConnexionUtilisateur::connecter($utilisateur->getIdUtilisateur());
    }


    }