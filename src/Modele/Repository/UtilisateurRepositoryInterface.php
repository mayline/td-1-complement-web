<?php

namespace TheFeed\Modele\Repository;

use PDOStatement;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\DataObject\Utilisateur;

interface UtilisateurRepositoryInterface
{
    public function recuperer(): array;

    public function recupererParClePrimaire($id): ?Utilisateur;

    public function recupererParLogin($login);

    public function recupererParEmail($email);

    public function ajouter($entite);

    public function mettreAJour($entite);

    public function supprimer($entite);

    public function extraireUtilisateur(PDOStatement $statement, array $values);
}