<?php

namespace TheFeed\Modele\Repository;

use PDO;

interface ConnexionBaseDeDonneesInterface
{
    public function getPdo(): PDO;
}