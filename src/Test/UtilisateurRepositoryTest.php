<?php

namespace TheFeed\Test;

use PHPUnit\Framework\TestCase;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\ConnexionBaseDeDonnees;
use TheFeed\Modele\Repository\ConnexionBaseDeDonneesInterface;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;

class UtilisateurRepositoryTest extends TestCase
{
    private static UtilisateurRepositoryInterface  $utilisateurRepository;

    private static ConnexionBaseDeDonneesInterface $connexionBaseDeDonnees;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$connexionBaseDeDonnees = new ConnexionBaseDeDonnees(new ConfigurationBDDTestUnitaire());
        self::$utilisateurRepository = new UtilisateurRepository(self::$connexionBaseDeDonnees);
    }

    protected function setUp(): void // beforeEachTest
    {
        parent::setUp();
        self::$connexionBaseDeDonnees->getPdo()->query("INSERT INTO 
                                                         utilisateurs (idUtilisateur, login, mdpHache, email, nomPhotoDeProfil) 
                                                         VALUES (1, 'test', 'test', 'test@example.com', 'test.png')");
        self::$connexionBaseDeDonnees->getPdo()->query("INSERT INTO 
                                                         utilisateurs (idUtilisateur, login, mdpHache, email, nomPhotoDeProfil) 
                                                         VALUES (2, 'test2', 'test2', 'test2@example.com', 'test2.png')");
    }

    public function testRecuperer() {
        $this->assertCount(2, self::$utilisateurRepository->recuperer());
    }

    public function testRecupererParClePrimaire() {
        $utilisateur = self::$utilisateurRepository->recupererParClePrimaire(1);
        $this->assertEquals(1, $utilisateur->getIdUtilisateur());
        $this->assertEquals('test', $utilisateur->getLogin());
        $this->assertEquals('test', $utilisateur->getMdpHache());
        $this->assertEquals('test@example.com', $utilisateur->getEmail());
        $this->assertEquals('test.png', $utilisateur->getNomPhotoDeProfil());
    }

    public function testRecupererParLogin() {
        $utilisateur = self::$utilisateurRepository->recupererParLogin('test2');
        $this->assertEquals(2, $utilisateur->getIdUtilisateur());
        $this->assertEquals('test2', $utilisateur->getLogin());
        $this->assertEquals('test2', $utilisateur->getMdpHache());
        $this->assertEquals('test2@example.com', $utilisateur->getEmail());
        $this->assertEquals('test2.png', $utilisateur->getNomPhotoDeProfil());
    }

    public function testRecupererParEmail() {
        $utilisateur = self::$utilisateurRepository->recupererParEmail('test@example.com');
        $this->assertEquals(1, $utilisateur->getIdUtilisateur());
        $this->assertEquals('test', $utilisateur->getLogin());
        $this->assertEquals('test', $utilisateur->getMdpHache());
        $this->assertEquals('test@example.com', $utilisateur->getEmail());
        $this->assertEquals('test.png', $utilisateur->getNomPhotoDeProfil());
    }

    public function testAjouter() {
        $utilisateur = Utilisateur::create('test3', 'test3', 'test3@example.com', 'test3.png');
        $utilisateur->setIdUtilisateur(3);
        self::$utilisateurRepository->ajouter($utilisateur);
        $this->assertCount(3, self::$utilisateurRepository->recuperer());
    }

    public function testMettreAJour() {
        $utilisateur = Utilisateur::create('test4', 'test4', 'test4@example.com', 'test4.png');
        $utilisateur->setIdUtilisateur(2);
        self::$utilisateurRepository->mettreAJour($utilisateur);
        $utilisateur = self::$utilisateurRepository->recupererParLogin('test4');
        $this->assertEquals(2, $utilisateur->getIdUtilisateur());
        $this->assertEquals('test4', $utilisateur->getLogin());
        $this->assertEquals('test4', $utilisateur->getMdpHache());
        $this->assertEquals('test4@example.com', $utilisateur->getEmail());
        $this->assertEquals('test4.png', $utilisateur->getNomPhotoDeProfil());
    }

    public function testSupprimer() {
        $utilisateur = Utilisateur::create('test2', 'test2', 'test2@example.com', 'test2.png');
        $utilisateur->setIdUtilisateur(2);
        self::$utilisateurRepository->supprimer($utilisateur);
        $this->assertCount(1, self::$utilisateurRepository->recuperer());

        $utilisateur = Utilisateur::create('test', 'test', 'test@example.com', 'test.png');
        $utilisateur->setIdUtilisateur(1);
        self::$utilisateurRepository->supprimer($utilisateur);
        $this->assertCount(0, self::$utilisateurRepository->recuperer());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        self::$connexionBaseDeDonnees->getPdo()->query("DELETE FROM utilisateurs");
    }

}