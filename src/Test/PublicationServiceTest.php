<?php

namespace TheFeed\Test;

use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use TheFeed\Controleur\ControleurPublication;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepositoryInterface;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;

class PublicationServiceTest extends TestCase
{
    private PublicationService $service;
    private PublicationRepositoryInterface $publicationRepositoryMock;
    private UtilisateurRepositoryInterface $utilisateurRepositoryMock;

    //On réinitialise l'ensemble avant chaque test

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->publicationRepositoryMock = $this->createMock(PublicationRepositoryInterface::class);
        $this->utilisateurRepositoryMock = $this->createMock(UtilisateurRepositoryInterface::class);
        $this->service = new PublicationService($this->publicationRepositoryMock, $this->utilisateurRepositoryMock);
    }

    public function testCreerPublicationUtilisateurInexistant()
    {
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Il faut être connecté pour publier un feed");
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn(null);
        $this->service->creerPublication(2,"test");
    }

    public function testCreerPublicationVide()
    {
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Le message ne peut pas être vide!");
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn(new Utilisateur());
        $this->service->creerPublication(2,"");
    }

    public function testCreerPublicationTropGrande()
    {
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Le message ne peut pas dépasser 250 caractères!");
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn(new Utilisateur());
        $this->service->creerPublication(2,str_repeat("a", 251));
    }
    public function testNombrePublications()
    {
        $this->publicationRepositoryMock->method("recuperer")->willReturn(array("fakePublication1", "fakePublication2"));
        $nbPublicationsRecuperer = $this->service->recupererPublications();
        self::assertCount(2, $nbPublicationsRecuperer);
    }

    public function testNombrePublicationsUtilisateur()
    {
        $this->publicationRepositoryMock->method("recupererParAuteur")->willReturn(array("fakePublication1", "fakePublication2"));
        $nbPublicationsRecuperer = $this->service->recupererPublicationsUtilisateur(1);
        self::assertCount(2 ,$nbPublicationsRecuperer);
    }

    public function testNombrePublicationsUtilisateurInexistant()
    {
        $this->publicationRepositoryMock->method("recupererParAuteur")->willReturn(array());
        $nbPublicationsRecuperer = $this->service->recupererPublicationsUtilisateur(-1);
        self::assertCount(0 ,$nbPublicationsRecuperer);
    }

    public function testCreerPublicationValide()
    {
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn(new Utilisateur());
        $this->publicationRepositoryMock->method("ajouter")->willReturn(null);
        $this->assertNull($this->service->creerPublication(2, "message Valide"));
    }







}