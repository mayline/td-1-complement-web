<?php

////////////////////
// Initialisation //
////////////////////
use Symfony\Component\HttpFoundation\Request;
use TheFeed\Controleur\RouteurURL;

require_once __DIR__ . '/../vendor/autoload.php';

/////////////
// Routage //
/////////////

$requete = Request::createFromGlobals();
$reponse = RouteurURL::traiterRequete($requete);
$reponse->send();
